---
title: "IterativeSpatialSubsample"
author: "Chloe Bellamy"
date: "Nov 2019"
output: html_document
---


```{r "setup", cache = FALSE}
.libPaths("C:/R-Packages")#set library path
require("knitr")

opts_knit$set(root.dir = root.dir)
knitr::opts_chunk$set(fig.width=8, fig.height=8, fig.path='Figs/', #set figure options
                      echo=TRUE, warning=FALSE, message=FALSE)


```

##Load required packages
```{r "load packages", cache = FALSE, warning = FALSE}
#Packages
package.list = c("tidyverse",  "dplyr", "plyr", "sp", "spatialEco", "rgdal")
tmp.install = which(lapply(package.list, require, character.only = TRUE)==FALSE)
if(length(tmp.install)>0) install.packages(package.list[tmp.install])
lapply(package.list, require, character.only = TRUE)


#Directories
#Directories - create outputs and scratch folders if they don't already exist in the root directory
ifelse(!dir.exists(file.path(root.dir, "Scratch")), dir.create(file.path(root.dir, "Scratch")), FALSE)
ifelse(!dir.exists(file.path(root.dir, "Outputs")), dir.create(file.path(root.dir, "Outputs")), FALSE)

```

##Iterative subsampling
Split data into subsamples without repalcement whilst ensuring minimum distance between observations in a group.
```{r "subsample"}
gc()#clear memory

sp::coordinates(InputData) <- ~ x+y #set coordinates

subsample = 2000 #number of observations to sample
distance = 3000 #minimum distance between sections in a sample (m)
maxFolds = floor(nrow(InputData)/subsample) #round(SampleSize/subsample) 
subsample_list = list() #create list for subsamples
dm_list = list()#create list for minimum distance values

# Iteratively subsample without replacement: Select subsample n sections each prescribed distance apart 
subbed = InputData #copy data
subbed$loc = paste (subbed$x, subbed$y, sep ="")#add location column

for (i in 1:maxFolds) { 
  sub.Inputs <- spatialEco::subsample.distance(subbed, n = subsample, d = distance, replacement = F, trace =T)  
    plot(subbed,pch=19, main= paste ("min dist = ", distance))
      points(sub.Inputs, pch=19, col="red") 
        sub.Inputs$fold = i
          subsample_list[[i]] = as.data.frame(sub.Inputs)
  
  # Check distances	
  dm <- spDists(sub.Inputs)
  diag(dm) <- NA
  dm_list [[i]] = as.data.frame(min(dm, na.rm=TRUE)) #Min distance for subsample 
  
#Remove the sampled points from input data
  subbed = subbed[!(subbed$loc %in% sub.Inputs$loc),]
  
 }

#Combine data with new "fold" column
SubbedData = bind_rows(subsample_list) 
     write.csv(SubbedData, paste ("Inputs/Subbed_Inputs_Sept", Sys.Date(), subsample, ".csv", sep="_"), quote=FALSE, row.names=FALSE)

#Calculate stats on the minimum distance values between data in each fold
MinDists = bind_rows(dm_list)  
    SubsampleStat   = data.frame(matrix(NA, ncol=4, nrow=1)) 
    colnames(SubsampleStat) =c("MinD","MaxD", "MeanD", "SdD")
    SubsampleStat$MinD = min(MinDists[,1])
    SubsampleStat$MaxD = max(MinDists[,1])
    SubsampleStat$MeanD = mean(MinDists[,1])
    SubsampleStat$SdD = sd(MinDists[,1])
    
plot(SubbedData$fold)
    
```

Check teh size of each fold and remove if the sample size is too small
```{r "remove folds"}
#Check size of each fold
hist(SubbedData$fold)
length(which(SubbedData$fold == 11))  

#Remove any folds with too few samples
SubbedData = subset(SubbedData, fold<11)
```

