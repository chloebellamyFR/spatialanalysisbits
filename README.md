Description
==============

**Various bits of code for carrying out (largely spatial) analysis in R**

Corresponding author
--------------
Chloe Bellamy **chloe.bellamy@forestresearch.gov.uk**

Please get in touch with any questions.

https://www.forestresearch.gov.uk/staff/chloe-bellamy/ 


IterativeSpatialSubsample.Rmd
--------------
This R code allows the user to iteratively subsample a set of spatial data points without replacement, so that the points within each data fold are all a set distance apart from one another. The user sets these parameters:

**Parameters**

subsample = 2000 #number of observations to sample (added to a chunk)

distance = 3000 #minimum distance between sections in a sample (m)

The maximum possible number of folds is calculated as maxFolds = floor(nrow(InputData)/subsample) 

The user may then want to remove folds with too small a sample size (e.g. InputData = subset(InputDataSubbed, fold<11))






